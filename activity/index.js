let numInput = Number(prompt("Please provide a number"));

console.log(`The number you provided is ${numInput}`);
for (; numInput >= 0; numInput--) {
  if (numInput <= 50) {
    console.log("The current value is at 50. Terminating the loop.");
    break;
  }
  if (numInput % 10 === 0) {
    console.log("Current number is divisible by 10. Skipping the number.");
    continue;
  }
  if (numInput % 5 === 0) {
    console.log(numInput);
  }
}

let word = "supercalifragilisticexpialidocious";
let newWord = " ";
console.log(word);
for (let i = 0; i < word.length; i++) {
  if (
    word[i].toLowerCase() == "a" ||
    word[i].toLowerCase() == "e" ||
    word[i].toLowerCase() == "i" ||
    word[i].toLowerCase() == "o" ||
    word[i].toLowerCase() == "u"
  ) {
    continue;
  } else {
    newWord += word[i];
  }
}
console.log(newWord);

// Repetition Control Structures
/* 
  Commonly known as Loops
  - lets us execute code repeatedly in a pre-set number of time or maybe forever(infinite loop - AVOID)
  - types
      while loop 
        it takes in an expression/condition before proceeding in the evaluation of the codes 
        syntax
          while(expression/condition){
            statement/s; 
          }

      do while loop
        executes codes within a certain number of times 
        at least one code block will be executed before proceeding to the condition 
        syntax
          do{
            statement/s;
          } while(condition)

      for loop 
        more flexible looping structure 
        syntax
          for(initialization; condition; finalExpression){
            statement/s; 
          }

      continue and break statements
        continue 
          keyword that allows code to go to the next iteration of the loop without finishing the execution of all statement in a code block 
          does not cause an infinite loop 
          continues onto the next iteration without finishing the rest of the code block 
        break 
          keyword that terminates the current loop completely once a match has been found 
          
*/

// While loop
let count = 5;

while (count !== 0) {
  console.log("While loop: " + count);
  // this ^ statement above will cause an infinite loop because it will always be true; need an expression to renew the value of the count variable

  count--; // decrements the value after the console.log is executed; **code is read from top to bottom**
}

// Mini activity
count = 1;
while (count <= 10) {
  console.log(count);
  count++;
}

//Do-While loop
let countTwo = 5;

do {
  console.log("Do-while loop " + countTwo); // will be executed at least once if the condition isn't satisfied
  countTwo--;
} while (countTwo > 0);
//  while (countTwo > 5);

//Mini activity
countTwo = 1;
do {
  console.log(countTwo);
  countTwo++;
} while (countTwo <= 10);

// For loop
for (let countThree = 5; countThree > 0; countThree--) {
  console.log("For loop: " + countThree); // will initialize then go to condition then will execute this expression first then go to the final expression then goes back to the condition then repeat
}

/* let number = Number(prompt("Give me a number")); //number works the same as parseInt

for (let numCount = 1; numCount <= number; numCount++) {
  console.log("Hello Batch 144");
} // if the number inputted is smaller than the numCount then it will repeat the statement until it reaches the number given */

let myString = "alex"; //can treat like an array of strings
// console.log(myString.length);

// console.log(myString[2]); //index - location of values in the array
//length - number of values in the array

for (let x = 0; x < myString.length; x++) {
  console.log(myString[x]);
}

let myName = "Alex";
for (let i = 0; i < myName.length; i++) {
  if (
    myName[i].toLowerCase() == "a" ||
    myName[i].toLowerCase() == "e" ||
    myName[i].toLowerCase() == "i" ||
    myName[i].toLowerCase() == "o" ||
    myName[i].toLowerCase() == "u"
    //if letter is a vowel will print 3
  ) {
    console.log(3);
  } else {
    console.log(myName[i]);
    //if consonant will print the value
  }
}

//continue and break keyword
for (let count = 0; count <= 20; count++) {
  if (count % 2 === 0) {
    //if remainder is equal to 0
    continue; //code will continue to the next iteration of the loop; skips the rest of the code below to the next iteration
  }
  //current value of the number is printed out if the remainder is not equal to 0
  console.log("Continue and Break: " + count);
  // if the current value of count is greater than 10
  if (count > 10) {
    break; //tells code to terminate the loop even if the loop condition is still being satisfied
  }
} //last value of this loop will be 11 because 10 is skipped and the console.log comes before the break keyword

let name = "Alexandro";
for (let i = 0; i < name.length; i++) {
  console.log(name[i]);

  if (name[i].toLowerCase() === "a") {
    console.log("Continue to the next iteration");
    continue;
  }
  if (name[i].toLowerCase() === "d") {
    break;
  }
}
